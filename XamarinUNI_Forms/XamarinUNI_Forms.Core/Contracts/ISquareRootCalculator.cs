﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinUNI_Forms.Core.Contracts
{
    public interface ISquareRootCalculator
    {
        double Calculate(double numberToCalculate);
    }
}
