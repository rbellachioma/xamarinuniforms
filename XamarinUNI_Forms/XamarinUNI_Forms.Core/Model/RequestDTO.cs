﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinUNI_Forms.Core.Model
{
    public class RequestDTO
    {
        public double numToCalculate { get; set; }
        public double pow { get; set; }
        public string username { get; set; }
    }
}
