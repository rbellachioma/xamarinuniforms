﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinUNI_Forms.Core.Model
{
    public class ResponseDTO
    {
        public double result { get; set; }
        public string errorDescription { get; set; }
        public bool inError { get; set; }
    }
}
