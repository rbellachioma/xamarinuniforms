﻿using MvvmCross.Forms.Platform;
using Xamarin.Forms;
using XamarinUNI_Forms.Core.Pages;

namespace XamarinUNI_Forms.Core
{
    public partial class Application: MvxFormsApplication
    {
        public NavigationPage NavigationPage { get; private set; }
        public Application()
        {
            InitializeComponent();
        }
    }
}
