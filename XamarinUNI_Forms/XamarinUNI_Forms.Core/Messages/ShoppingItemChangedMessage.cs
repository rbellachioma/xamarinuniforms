﻿using MvvmCross.Plugins.Messenger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinUNI_Forms.Core.Messages
{
    public class ShoppingItemChangedMessage : MvxMessage
    {
        public ShoppingItemChangedMessage(object sender) : base(sender)
        {
        }
    }
}
