﻿using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using XamarinUNI_Forms.Core.Contracts;
using XamarinUNI_Forms.Core.Messages;
using XamarinUNI_Forms.Core.Model;

namespace XamarinUNI_Forms.Core.ViewModels
{
    public class ShoppingListViewModel : MvxViewModel
    {
        private bool isShoppingListRefreshing = false;
        public bool IsShoppingListRefreshing
        {
            get { return isShoppingListRefreshing; }
            set { SetProperty(ref isShoppingListRefreshing, value); }
        }

        private bool hasError = false;

        public bool HasError
        {
            get { return hasError; }
            set { SetProperty(ref hasError, value); }
        }

        private string errorDesc = string.Empty;

        public string ErrorDesc
        {
            get { return errorDesc; }
            set { SetProperty(ref errorDesc, value); }
        }

        public ObservableCollection<ShoppingItemViewModel> ShoppingItems { get; }
        readonly MvxSubscriptionToken token;
        readonly IWebClientService webClientService;
        readonly IMvxNavigationService navigationService;
        readonly IMvxMessenger messenger;
        public IMvxAsyncCommand ShowAddNewShoppingItemCommand { get; }
        public IMvxAsyncCommand RefreshItemsCommand { get; }

        public ShoppingListViewModel(IWebClientService webClientService, IMvxNavigationService navigationService, IMvxMessenger messenger)
        {
            ShoppingItems = new ObservableCollection<ShoppingItemViewModel>();
            ShowAddNewShoppingItemCommand = new MvxAsyncCommand(ShowAddNewItem);
            RefreshItemsCommand = new MvxAsyncCommand(LoadShoppingItems);
            this.webClientService = webClientService;
            this.navigationService = navigationService;
            this.messenger = messenger;
            token = messenger.SubscribeOnMainThread<ShoppingItemChangedMessage>(async m => await LoadShoppingItems());
        }

        public override async void Start()
        {
            base.Start();
            await LoadShoppingItems();
        }

        public async Task LoadShoppingItems()
        {
            try
            {
                IsShoppingListRefreshing = true;
                HasError = false;
                ErrorDesc = string.Empty;

                ShoppingItems.Clear();
                var newItems = await webClientService.getAllShoppingItems();
                foreach (var item in newItems)
                {
                    var viewModel = new ShoppingItemViewModel(webClientService, navigationService, messenger);
                    await viewModel.Initialize(item);
                    ShoppingItems.Add(viewModel);
                }
                IsShoppingListRefreshing = false;
            }
            catch (Exception)
            {
                IsShoppingListRefreshing = false;
                ErrorDesc = "Non riesco a contattare il ws";
                HasError = true;
            }
        }

        async Task ShowAddNewItem()
        {
            await navigationService.Navigate<ShoppingItemViewModel, ShoppingItem>(new ShoppingItem());
        }
    }
}
