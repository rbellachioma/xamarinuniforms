﻿using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using MvvmCross.Plugins.Messenger;
using XamarinUNI_Forms.Core.Contracts;
using XamarinUNI_Forms.Core.Services;

namespace XamarinUNI_Forms.Core
{
    public class CoreApp : MvvmCross.Core.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
            Mvx.LazyConstructAndRegisterSingleton<ISquareRootCalculator, SquareRootCalculator>();
            Mvx.LazyConstructAndRegisterSingleton<IMvxMessenger, MvxMessengerHub>();
            RegisterNavigationServiceAppStart<ViewModels.ShoppingListViewModel>();
        }
    }
}
