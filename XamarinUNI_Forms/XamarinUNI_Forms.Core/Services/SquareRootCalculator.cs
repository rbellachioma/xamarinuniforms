﻿using XamarinUNI_Forms.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinUNI_Forms.Core.Services
{
    public class SquareRootCalculator : ISquareRootCalculator
    {
        public double Calculate(double numberToCalculate)
        {
            Task.Delay(TimeSpan.FromSeconds(10)).Wait();
            return Math.Sqrt(numberToCalculate);
        }
    }
}
